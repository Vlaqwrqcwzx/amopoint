<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
    <title>Form</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12 p-3">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Загрузите txt* файл</h4>
                    <p class="card-text">
                    <form id="loadForm" class="form-inline" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2">
                                <input name="file" type="file" class="form-control-file" id="file"
                                       accept=".txt"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="separator">Разделитель</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <input name="separator" type="text" class="form-control" id="separator"
                                       placeholder="Разделитель">
                            </div>
                        </div>
                        <input type="hidden" name="MAX_FILE_SIZE" value="30000"/>
                        <button type="submit" class="btn btn-primary mb-2">Отправить</button>
                    </form>
                    </p>
                </div>
                <div class="card-body" id="result"></div>
            </div>
        </div>
    </div>
</div>

<div id="loader">
    <img src="assets/images/loader.gif" alt="">
</div>

<script src="assets/js/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script>
    $(document).ready(function () {
        $('#loadForm').submit(function (event) {
            event.preventDefault();

            var file = $('[name="file"]').prop('files')[0];
            var separator = $('[name="separator"]').val();
            var data = new FormData();
            data.append('file', file);
            data.append('separator', separator);

            $.ajax({
                type: 'POST',
                url: 'load.php',
                data: data,
                dataType: "json",
                processData: false,
                contentType: false,
                processing: function () {
                    $('#loader').show();
                },
                success: function (data) {
                    $('#loader').hide()
                    if (data.message == 'success') {
                        toastr.success('Файл загружен')
                        $('#result').empty();
                        for (let key in data.result) {
                            $('#result').append('<p>Строка ' + key + ' =  количество цифр в строке ' + data.result[key]);
                        }
                    } else {
                        toastr.error('Произошла ошибка')
                    }
                }
            });

        });
    })
</script>
</body>
</html>