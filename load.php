<?php

if (empty($_FILES['file']['tmp_name'])) {
    echo json_encode(['message' => 'error']);
} else {
    $path = 'upload/' . $_FILES['file']['name'];
    move_uploaded_file($_FILES['file']['tmp_name'], $path);


    $explodeArr = explode($_POST['separator'], file_get_contents($path));

    foreach ($explodeArr as $item) {
        $resultArr[$item] = strlen(preg_replace('/[^\d]/','',$item));
    }

    echo json_encode([
        'message' => 'success',
        'result' => $resultArr,
        ]);
}