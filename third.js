$('[name="type_val"]').on('change', function (event) {
    let type_id = $(this).val();
    $('input').closest('p').hide();

    $('input').each(function (index, value) {
        if ($(this).attr('name').indexOf(type_id) != -1) {
            $(this).closest('p').show();
        }
    });
})